/**
 * Created by DICH on 3/11/2017.
 */
var ServiceCaller = require('../lib/service_caller');
var path = require('path');
const DEFAULT_REMOTE_SERVICE_CONFIG_FILE_NAME = 'remote_service.config.json';

/**
 * load broker config file and init connect
 * @param app
 * @param rootDir where application start
 * @param {error} callback
 */
module.exports = function (app, rootDir, callback) {
    rootDir = rootDir || './';

    var env = (process.env.NODE_ENV || 'dev');

    var remoteServiceConfigFile = 'remote_service.config.' + env + '.json';

    try {
        var brokersInfos = require(path.join(rootDir, remoteServiceConfigFile));
    } catch (e) {
        brokersInfos = require(path.join(rootDir, DEFAULT_REMOTE_SERVICE_CONFIG_FILE_NAME));
    }

    app.serviceCaller = ServiceCaller(brokersInfos, rootDir);

    callback(null);
}
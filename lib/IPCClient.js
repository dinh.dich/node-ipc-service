/**
 * Created by Dich on 3/14/2016.
 */

var IPCNetClient = require('./IPCNetClient');
var inherits = require('util').inherits;
var EventEmitter = require('events').EventEmitter;
var States = require('../constants/ServiceState');
var heartbeats = require('heartbeats')
var heart

var IPCClient = function (serverId, host, port, secretKey) {
    this.serverId = serverId;
    this.ipc = require('node-ipc');
    this.ipc.config.id   = serverId;
    this.ipc.config.retry= 500;
    this.ipc.config.silent = true;
    this.cbMap = {};
    this.cbIndex = -1;
    this.connected = false;
    this.state = States.WAITING_CONNECT;
    var self = this;

    this.ipc.connectTo(this.serverId, function () {
        self.ipc.of[self.serverId].on('connect', function () {
            console.log('connected to ' + serverId + ' server');
            self.server = self.ipc.of[self.serverId];
            self.connected = true;
            self.state = States.CONNECTED;
            self.emit('connected', serverId);

            //call heartdeats every 30 seconds
            heart = heartbeats.createHeart(30 * 1000)
            heart.createEvent(1, function(count, last){
                self.callMethod('ipc.heartbeats', [], function (error, receiveTime) {
                    //TODO
                })
            })
        });

        self.ipc.of[self.serverId].on('disconnect', function () {
            self.state = States.DISCONNECT;
            self.emit('disconnect', serverId);

            self.connected = self.getState() == States.CONNECTED;
            heart && heart.kill()
        });

        self.ipc.of[self.serverId].on('error', function (error) {
            self.state = States.DISCONNECT;
            self.emit('error', error);

            self.connected = self.getState() == States.CONNECTED;
            heart && heart.kill()
        });

        self.ipc.of[self.serverId].on('callback', function (data) {
            var callId = data.call_id;
            var error = data.res.error;
            var result = data.res.result;
            if(callId && self.cbMap[callId]) {
                self.cbMap[callId](error, result);
                delete self.cbMap[callId];
            }
        });

        self.ipc.of[self.serverId].on('cb_method', function (data) {
            var callId = data.call_id;
            var error = data.res.error;
            if(callId && self.cbMap[callId]) {
                self.cbMap[callId](error, data.res);
                delete self.cbMap[callId];
            }
        });
    });

    if(host && host != '' && port && port != '') {
        this.netClient = new IPCNetClient('net-' + serverId, host, port, secretKey);
        this.netClient.on('connected', function () {
            self.connected = true;
            self.netState = States.CONNECTED;
            self.emit('connected', 'net-' + serverId);
        })

        this.netClient.on('disconnect', function () {
            self.netState = States.DISCONNECT;
            self.emit('disconnect', 'net-' + serverId);

            self.connected = self.getState() == States.CONNECTED;
        })

        this.netClient.on('error', function () {
            self.netState = States.DISCONNECT;
            self.emit('error', 'net-' + serverId);

            self.connected = self.getState() == States.CONNECTED;
        })
    }

    this.on('connected', function () {
        //TODO waiting to override by client
    })

    this.on('disconnect', function () {
        //TODO waiting to override by client
    })

    this.on('error', function () {
        //TODO waiting to override by client
    })
};

inherits(IPCClient, EventEmitter);

/**
 *
 * @param method
 * @param arguments
 * @param params
 * @param {error, data} callback
 */
IPCClient.prototype.call = function (method, params, callback) {
    var self = this;

    if(!self.server || self.state != States.CONNECTED) {
        if(self.netClient) {
            return self.netClient.call(method, params, callback);
        } else {
            var error = new Error('service ' + self.serverId + ' not exist!');
            return callback(error);
        }
    }

    var message = {
        method: method,
        params: params
    };

    if(callback && typeof callback === 'function') {
        self.cbIndex += 1;
        if (self.cbIndex < 0) {
            self.cbIndex = 0;
        }

        var callId = self.cbIndex + '';
        message.call_id = callId;
        self.cbMap[callId] = callback;
    } else {
        callback = function () {};
    }

    if(self.server) {
        self.server.emit('call', message);
    } else {
        callback(new Error('service ' + self.serverId + ' not defined!'));
    }
};

/**
 *
 * @param method
 * @param params
 * @param {error, data} callback
 */
IPCClient.prototype.callMethod = function (method, params, callback) {
    var self = this;

    if(!self.server || self.state != States.CONNECTED) {
        if(self.netClient) {
            return self.netClient.callMethod(method, params, callback);
        } else {
            var error = new Error('service ' + self.serverId + ' not exist!');
            callback(error);
            return;
        }
    }

    var message = {
        method: method,
        params: params
    };

    if(callback && typeof callback === 'function') {
        self.cbIndex += 1;
        if (self.cbIndex < 0) {
            self.cbIndex = 0;
        }

        var callId = self.cbIndex + '';
        message.call_id = callId;
        self.cbMap[callId] = callback;
    } else {
        callback = function () {}
    }

    if(self.server) {
        self.server.emit('call_method', message);
    } else {
        callback(new Error('service ' + self.serverId + ' not defined!'));
    }
};

/**
 *
 * @returns {boolean}
 */
IPCClient.prototype.isConnected = function () {
    return this.connected;
};

/**
 *
 */
IPCClient.prototype.disconnect = function () {
    this.ipc.disconnect(this.serverId);
}

/**
 *
 * @return {state}
 */
IPCClient.prototype.getState = function () {
    if(this.state == States.CONNECTED || this.netState == States.CONNECTED) {
        return States.CONNECTED;
    } else if(this.state == States.WAITING_CONNECT || this.netState == States.WAITING_CONNECT) {
        return States.WAITING_CONNECT;
    } else {
        return States.DISCONNECT;
    }
}

module.exports = IPCClient;
/**
 * Created by DICH on 7/9/2016.
 */
var cassandra  = require('cassandra-driver');
var IPCClient = require('../lib/IPCClient');
var IPCNetClient = require('../lib/IPCNetClient');
var fs = require('fs');
var path = require('path');
var State = require('../constants/ServiceState');

/**
 * after send data between process then TimeUuid type become string, so can not invoke getDate() function.
 */
String.prototype.getDate = function () {
    try {
        return cassandra.types.TimeUuid.fromString(this.toString()).getDate();
    } catch (e) {
        throw new Error('getDate is not a function.');
    }
};

/**
 * after send data between process then Date type become string, so can not invoke getTime() function.
 */
String.prototype.getTime = function () {
    try {
        return Date.parse(this.toString());
    } catch (e) {
        throw new Error('getTime is not a function.');
    }
};

var services = {};
var callServicesQueues = {};

var ServiceCaller = function (remoteServiceConfig, rootDir) {
    var connectService = function (serviceId, serviceInfo) {
        if(!serviceInfo.adapter || serviceInfo.adapter == 'node-ipc') {
            services[serviceId] = new IPCClient(serviceId, serviceInfo.host, serviceInfo.port, serviceInfo.secretKey);
        } else {
            let Adapter = require(serviceInfo.adapter) || require(path.join(rootDir, serviceInfo.adapter));
            services[serviceId] = new Adapter(serviceInfo);
        }
        services[serviceId].on('connected', function () {
            executeCallQueue(serviceId);
        })
    }

    for (var serviceId in remoteServiceConfig) {
        var serviceInfo = remoteServiceConfig[serviceId];
        connectService(serviceId, serviceInfo);
    }

    return ServiceCaller;
}

/**
 *
 * @param serviceName
 * @param method
 * @param params
 * @param {arguments} callback
 */
ServiceCaller.call = function (serviceName, method, params, callback) {
    if (!callback) {
        callback = function () {
        };
    }

    params = params || [];

    if (services[serviceName] && services[serviceName].isConnected()) {
        services[serviceName].callMethod(method, params, function (error, data) {
            if (error) {
                callback(error);
            } else {
                callback.apply(null, data.result);
            }
        });
    } else if(services[serviceName] && services[serviceName].getState() == State.WAITING_CONNECT) {
        if (!callServicesQueues[serviceName]) {
            callServicesQueues[serviceName] = [];
        }
        callServicesQueues[serviceName].push([serviceName, method, params, callback]);
    } else if(services[serviceName]) {
        callback(new Error('service ' + serviceName + ' has down!'))
    } else {
        callback(new Error('service ' + serviceName + ' must be defined in file config!'))
    }
};

/**
 *
 * @param serviceName
 * @return {*|boolean}
 */
ServiceCaller.isConnected = function (serviceName) {
    return services[serviceName] && services[serviceName].isConnected()
}

/**
 *
 * @param serviceName
 */
var executeCallQueue = function (serviceName) {
    if (callServicesQueues[serviceName]) {
        for (var i = 0; i < callServicesQueues[serviceName].length; i++) {
            var infos = callServicesQueues[serviceName].shift();
            ServiceCaller.call.apply(this, infos);
        }
    }
};

module.exports = ServiceCaller;
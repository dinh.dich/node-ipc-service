/**
 * Created by DICH on 3/11/2017.
 */
exports.IPCClient = require('./lib/IPCClient');
exports.IPCServer = require('./lib/IPCServer');
exports.serviceCaller = require('./lib/service_caller');
exports.boot = require('./lib/boot');